export interface InputDef {
  fieldName: string;
  inputLabel: string;
  type: string; // string, date, number, select, autocomplete
  options?: string[]; // for select and autocomplete types only
}
