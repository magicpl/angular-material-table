import {Component, EventEmitter, Input, Output} from '@angular/core';
import {InputDef} from './input-def';

@Component({
  selector: 'app-table-editor',
  templateUrl: './table-editor.component.html',
  styleUrls: ['./table-editor.component.scss']
})
export class TableEditorComponent {

  editedObject: any;
  inputsValue: any = {};

  @Output() editionFinished: EventEmitter<any> = new EventEmitter<any>();
  @Input() inputDefs: InputDef[];

  @Input() set edited(value: any) {
    this.editedObject = value;
    if (this.editedObject === null || this.editedObject === undefined) {
      this.inputsValue = {};
    } else {
      Object.assign(this.inputsValue, value);
    }
  }

  saveChanges() {
    this.convertNumberValues();
    this.editionFinished.emit(this.inputsValue);
    this.inputsValue = {};
    this.editedObject = null;
  }

  getValue(fieldName: string) {
    return String(fieldName);
  }

  filterAutocompleteValues(values: string[], value: string) {
    return value ? values.filter(option => option.toLowerCase().includes(value.toLowerCase())) : values;
  }

  private convertNumberValues() {
    this.inputDefs
      .filter(def => def.type === 'number')
      .forEach(def => this.inputsValue[def.fieldName] = Number(this.inputsValue[def.fieldName]));
  }
}
