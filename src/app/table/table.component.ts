import {Component, OnInit, ViewChild} from '@angular/core';
import {Employee} from './employee';
import {InputDef} from '../table-editor/input-def';
import {MatSort} from '@angular/material/sort';
import {EMPLOYEE_DATA} from './employee-data';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  selected: Employee;
  columnsToDisplay: string[] = ['firstName', 'lastName', 'sex', 'age', 'nationality', 'contractExpiration', 'lastVisit'];
  dataSource = new MatTableDataSource(EMPLOYEE_DATA);
  editableFields: InputDef[] = [
    {fieldName: 'firstName', inputLabel: 'Name', type: 'string'},
    {fieldName: 'lastName', inputLabel: 'Last Name', type: 'string'},
    {fieldName: 'sex', inputLabel: 'Sex', type: 'select', options: ['Male', 'Female', 'Yes, I like']},
    {fieldName: 'age', inputLabel: 'Age', type: 'number'},
    {fieldName: 'nationality', inputLabel: 'Nationality', type: 'autocomplete', options: ['PL', 'DE', 'SK', 'US']},
    {fieldName: 'contractExpiration', inputLabel: 'Contract Expires', type: 'date'}
  ];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  getAgeMean() {
    return this.dataSource.data.reduce((a, b) => (a + b.age) / this.dataSource.data.length, 0);
  }

  setSelected(element: Employee) {
    if (this.selected === element) {
      this.selected = null;
    } else {
      this.selected = element;
    }
  }

  onEditionFinished(event: Employee) {
    Object.assign(this.selected, event);
  }
}

