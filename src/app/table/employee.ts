export class Employee {
  firstName: string;
  lastName: string;
  sex: string;
  age: number;
  nationality;
  contractExpiration: Date;
  lastVisit: Date;


  constructor(firstName?: string, lastName?: string, sex?: string, age?: number, nationality?, contractExpiration?: Date, lastVisit?: Date) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.sex = sex;
    this.age = age;
    this.nationality = nationality;
    this.contractExpiration = contractExpiration;
    this.lastVisit = lastVisit;
  }
}
